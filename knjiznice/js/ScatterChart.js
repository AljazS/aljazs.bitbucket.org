google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart(podatki) {

    var data = google.visualization.arrayToDataTable(podatki);
    
    var options = {
        title: 'Sistolični krvni tlak in Diastolični krvni tlak',
        hAxis: {title: 'Sistolični tlak mm[Hg]', minValue: 50, maxValue: 200, gridlines: {count: 15, color: 'grey'}},
        vAxis: {title: 'Diastolični tlak mm[Hg]', minValue: 50, maxValue: 150, gridlines: {count: 10, color: 'grey'}},
        legend: 'none',
        chartArea: {width:'600px', height:'600px'},
    };

    var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
    
    chart.draw(data, options);
}