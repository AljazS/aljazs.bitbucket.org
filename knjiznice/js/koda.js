var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
             "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */


function generiranjePodatkov() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);

    alert("Osebe uspešno generirane");
}

function generirajPodatke(stPacienta) {
    var sessionId = getSessionId();

    var ime;
    var priimek;
    var datumRojstva;
    var datumInUra;
    
    var SKT;
    var DKT;

    switch(stPacienta) {
        case 1:
            ime = "Ivan";
            priimek = "Vomit";
            datumRojstva = "2006-06-06";
            datumInUra = [ "2018-05-02T07:54", "2018-05-03T07:49", "2018-05-04T08:01", "2018-05-05T07:53", "2018-05-06T07:55", "2018-05-07T07:51", "2018-05-08T07:59", "2018-05-09T07:54", "2018-05-10T07:56", "2018-05-11T07:52"];
    
            SKT = [142, 141, 144, 151, 141, 143, 149, 144, 145, 145];
            DKT = [83, 87, 85, 83, 88, 87, 85, 91, 90, 89 ];
            break;
        case 2:
            ime = "Ron";
            priimek = "Močni";
            datumRojstva = "1998-05-23";
            datumInUra = [ "2017-05-01T21:54", "2017-06-01T21:58", "2017-07-01T21:59", "2017-08-01T21:57", "2017-09-01T22:01", "2017-10-01T21:59", "2017-11-01T22:00", "2017-12-01T21:55", "2018-01-01T21:58", "2018-02-01T21:54"];
    
            SKT = [101, 105, 104, 98, 108, 99, 99, 103, 109, 110];
            DKT = [71, 73, 76, 69, 68, 72, 66, 77, 70, 75];
            break;
        case 3:
            ime = "Olivija";
            priimek = "Oredu";
            datumRojstva = "2003-08-12";
            datumInUra = [ "2018-03-20T23:12", "2018-03-27T23:07", "2018-04-03T23:04", "2018-04-10T23:08", "2018-04-17T23:08", "2018-04-24T23:11", "2018-05-01T23:12", "2018-05-08T23:10", "2018-05-15T23:09", "2018-05-22T23:13"];
    
            SKT = [82, 84, 82, 81, 81, 87, 89, 91, 88, 87];
            DKT = [61, 59, 55, 56, 58, 58, 58, 59, 60, 56];
            break;
        default:
            alert("Napaka: Napačna stPacienta");
    }
    
    
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });

    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };
            
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    
                    if (party.action == 'CREATE') {
                        
                        if(stPacienta == 1){
                            document.getElementById('pacienti').options[1].value = ehrId;
                            console.log("Nov zgeneriran ehrID za Ivan Vomit: "+ehrId);
                        }else
                        if(stPacienta == 2){
                            document.getElementById('pacienti').options[2].value = ehrId;
                            console.log("Nov zgeneriran ehrID za Ron Močni: "+ehrId);
                        }else
                        if(stPacienta == 3){
                            document.getElementById('pacienti').options[3].value = ehrId;
                            console.log("Nov zgeneriran ehrID za Olivija Oredu: "+ehrId);
                        }
                        for(var i=0; i<SKT.length; i++){
                            dodajPodatkeOsebe(sessionId, ehrId, datumInUra[i], SKT[i], DKT[i]);
                        }
                    }
                },
                error: function(err) {
                    if (err) {
                        console.log(err);
                        alert("Napaka: Generiranje EHR ID");
                    }
                }
            });
        }
    });
}

function dodajPodatkeOsebe(sessionId, ehrId, datumInUra, SKT, DKT) {
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    
    var podatki = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/blood_pressure/any_event/systolic": SKT,
        "vital_signs/blood_pressure/any_event/diastolic": DKT
    };
    
    var parametriZahteve = {
        templateId: 'Vital Signs',
        ehrId: ehrId,
        format: 'FLAT',
    };
    
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        
        success: function (res) {
            console.log("Dodana meritev");
        },
        
        error: function(err) {
            if (err) {
                 console.log("Napaka: Dodajanje meritve");
                 console.log(err);
            }
        }
    });
}

function preberiMeritve(sessionId, ehrId) {
    var meritev;
    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/blood_pressure",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        async: false,
        success: function (res) {
            for (var i in res) {
                meritev = res;
            }
        }
    });
    return meritev;
}


function prikazPodatkov() {
    var ehrId = $('#ehrId').val();

        var meritev = preberiMeritve(getSessionId(), ehrId);

        var opcije;
        for(var i in meritev){
            opcije += '<option value="'+meritev[i].diastolic+';'+meritev[i].systolic+'">'+meritev[i].time+'</option>';
        }

        $('#casMeritve').html(opcije);
        $('#rezultat').html('<br><b>Sistolični krvni tlak:</b> '+meritev[0].systolic+'<b> mm[Hg]</b>'+'<br><b>Diastolični krvni tlak:</b> '+meritev[0].diastolic+'<b> mm[Hg]</b>');


        var stMeritev = meritev.length;
        
        var data = [ ['', ''] ];
        
        var povprečenSKT = 0;
        var povprečenDKT = 0;
        
        for(var j=0; j<stMeritev; j++){
            data.push( [meritev[j].systolic, meritev[j].diastolic] );
            
            povprečenSKT = povprečenSKT + meritev[j].systolic;
            povprečenDKT = povprečenDKT + meritev[j].diastolic;
        }
        
        povprečenSKT = povprečenSKT / stMeritev;
        povprečenDKT = povprečenDKT / stMeritev;

        
        drawChart(data);    //graf

        $('#povprecje').html('<b>Povprečje </b>' + stMeritev + ' <b>meritev</b>'+ '<br> <b>Sistolični krvni tlak: </b>' + povprečenSKT+' <b>mm[Hg]</b>'+'<br> <b>Diastolični krvni tlak:</b> ' + povprečenDKT + ' <b>mm[Hg]</b>');

        var status;
        if(povprečenSKT >= 140 || povprečenDKT >= 90) {
            status = 'PREVISOK';
            
        }else if(povprečenSKT >= 120 || povprečenDKT >= 80) {
            status = 'POVIŠAN';
            
        }else if(povprečenSKT<=90 || povprečenDKT<=60) {
            status = 'PRENIZEK';
            
        }else{
            status = 'IDEALEN';
        }
        
        var izpisStatusa = '<b><br>Status vašega krvnega tlaka: '+status+'.</b><br>';
        $('#izpisStatusa').html(izpisStatusa);
        
        var izpisNasveta;
        var nasvet;
        
        $.ajax({
            url: 'knjiznice/zunanjiVir/nasveti.txt',
            async: false,
            success: function (csvd) {
                nasvet = csvd;
                console.log(nasvet);
            },
            dataType: "text",
        });
        
        nasvet = nasvet.split('\n');
        
        if(status == 'PREVISOK' || status == 'POVIŠAN'){
            //var stavek = nasvet.split('\n');
            //console.log(stavek[Math.floor((Math.random() * 12) + 0)]);
            
            //http://www.viva.si/Alternativna-in-naravna-pomo%C4%8D/7669/12-naravnih-re%C5%A1itev-za-visok-pritisk
            izpisNasveta = '<b>Nasvet: </b><br>'+ nasvet[Math.floor((Math.random() * 12) + 0)] +'<br>';
            
        }else if(status == 'PRENIZEK'){
            //http://www.zdravstvena.info/preventiva/krvni-pritisk-in-spremembe-zivljenjskega-stila-visok-krvni-pritisk-nizek-krvni-pritisk-krvni-tlak.html
            izpisNasveta = '<b>Nasvet je odvisen od vzroka, ki stanje nizkega pritiska povzroča, lahko zdravnik predlaga več sprememb, ki pomagajo pri zviševanju krvnega pritiska </b><br>'+ nasvet[Math.floor((Math.random() * 12) + 12)] +'<br>';
        }else if(status == 'IDEALEN'){
            
            izpisNasveta = 'Vaš krvni pritisk je <b>idealen</b>.';
        }
        
        $('#izpisNasveta').html(izpisNasveta);

}

$(document).ready(function() {

    //sprememba osebe naj zamenja ehrId
    $('#pacienti').change(function() {
        $('#ehrId').val($(this).val());
    });

    //sprememba datuma naj pokaze specificno meritev
    $('#casMeritve').change(function() {
        var tlak = $(this).val().split(';');
        $('#rezultat').html('<br>Diastolični krvni tlak: '+tlak[0]+' mm Hg'+
                           '<br>Sistolični krvni tlak: '+tlak[1]+' mm Hg');
    });

});
